az-rest-api-start
=================

To use this starter project, pull it and then change the git upstream to point to your new repo.

Features
--------

* Docker environment
* Nginx https server (self signed cert)
* MongoDB
* Mongoose models
* Unit testing with Mocha, Chai, and Supertest
* API resource generation with Mustache: model, routes and tests
* JWT authentication

Usage
-----

To run the api server
> docker-compose up

Now visit https://localhost to see it running.

There is an example resource called rock-star. But you'll need to authenticate to see it:
- Import the Postman collection json file into Postman
- Set a Postman variable called SERVER to 'https://localhost'
- POST to the /authenticate endpoint
- copy the token and refresh token from the response into TOKEN and REFRESH variables
- GET the collection at /rock-stars

To stop the api server
> Ctrl-c
> docker-compose down

To run commands inside the api docker container, use the script run.sh
> ./run.sh "some command"

For example, to generate a new resource
> ./run.sh "npm run resource"

Your resource will be created in the resources folder with default GET/POST/PUT/DELETE routes and tests

To run the tests in TDD mode
> ./test.sh up

To stop TDD
> Ctrl-c
> ./test.sh down

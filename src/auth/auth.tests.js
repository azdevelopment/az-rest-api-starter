const request = require('supertest')
const { app } = require('../server')
const User    = require('../resources/user/user.model')
const auth    = require('./auth.lib')
const chai    = require('chai')

const should  = chai.should()
chai.use(require('chai-shallow-deep-equal'))

describe('/authenticate', () => {
    let cleanUp = () =>
        User.remove({})

    beforeEach(cleanUp)
    after(cleanUp)

    describe('POST', () => {
        beforeEach(() =>
            User.create(auth.userData)
        )

        it('should return a jwt', () =>
            request(app)
                .post('/authenticate')
                .send({ email: auth.userData.email, password: auth.userData.password })
                .expect(200)
                .then(res => {
                    res.body.success.should.equal(true)
                    should.exist(res.body.token)
                    should.exist(res.body.refreshToken)
                })
        )

        it('should return an error if the password is incorrect', () =>
            request(app)
                .post('/authenticate')
                .send({ email: auth.userData.email, password: 'wrong password' })
                .expect(401)
        )

        it('should return an error if the user doesn\'t exist', () =>
            request(app)
                .post('/authenticate')
                .send({ email: 'wrong@email.az', password: auth.userData.password })
                .expect(401)
        )
    })
})

describe('/token', () => {
    beforeEach(() =>
        User.remove({})
    )

    describe('POST', () => {
        beforeEach(() =>
            User.create(auth.userData)
        )

        it('should return a new token', () =>
            request(app)
                .post('/authenticate')
                .send({ email: auth.userData.email, password: auth.userData.password })
                .expect(200)
                .then(res => {
                    res.body.success.should.equal(true)

                    return request(app)
                        .post('/token')
                        .send({ email: auth.userData.email, refreshToken: res.body.refreshToken })
                        .expect(200)
                        .expect(res => {
                            res.body.success.should.equal(true)
                            should.exist(res.body.token)
                        })
                })
        )

        it('should return an error if the refresh token is invalid', () =>
            request(app)
                .post('/token')
                .send({ email: auth.userData.email, refreshToken: 'abc' })
                .expect(401)
        )

        it('should return an error if the email sent doesn\'t match the refresh token\'s email', () =>
            request(app)
                .post('/authenticate')
                .send({ email: auth.userData.email, password: auth.userData.password })
                .expect(200)
                .then(res => {
                    res.body.success.should.equal(true)

                    return request(app)
                        .post('/token')
                        .send({ email: 'otheremail@email.az', refreshToken: res.body.refreshToken })
                        .expect(401)
                })
        )
    })
})

const request = require('supertest')
const { app } = require('../server')
const chai    = require('chai')

const User    = require('../resources/user/user.model')

const should  = chai.should()

const adminData = {
    email: 'admin@email.az',
    password: 'password',
    admin: true,
}

const userData = {
    email: 'user@email.az',
    password: 'password',
    admin: false,
}

const otherUserData = {
    email: 'other-user@email.az',
    password: 'password',
    admin: false,
}

const getToken = user =>
    request(app)
        .post('/authenticate')
        .send({ email: user.email, password: user.password })
        .expect(200)
        .then(res => {
            res.body.success.should.equal(true)
            should.exist(res.body.token)
            return res.body.token
        })


const getAdminToken = () =>
    User.create(adminData).then(user =>
        getToken(user)
    )

const getUserToken = () =>
    User.create(userData).then(user =>
        getToken(user)
    )

const getTokenAndUser = data =>
    User.create(data).then(user =>
        getToken(user).then(token => {
            return {
                user: user,
                token: token,
            }
        })
    )

const remove = () =>
    User.remove({})

module.exports = {
    getUserToken: getUserToken,
    getAdminToken: getAdminToken,
    getTokenAndUser: getTokenAndUser,
    adminData: adminData,
    userData: userData,
    otherUserData: otherUserData,
    remove: remove,
}

const jwt = require('jsonwebtoken')

const isAuthenticated = (req, res, next) => {
    let token = req.body.token || req.query.token || req.headers['x-access-token']

    // decode token
    if (token) {
        // verifies secret and checks exp
        jwt.verify(token, 'somesecret', (err, signingUser) => {
            if (err) {
                return res.status(401).send({
                    success: false,
                    message: 'Failed to authenticate token.'
                })
            } else {
                // if everything is good, save to request for use in other routes
                req.user = signingUser
                return next()
            }
        })
    } else {
        // if there is no token
        // return an error
        return res.status(401).send({
            success: false,
            message: 'No token provided.'
        })
    }
}

const isAuthorized = (req, res, next) => {
    if (req.user && req.user.admin) {
        return next()
    } else {
        return res.sendStatus(403)
    }
}

module.exports = {
    isAuthenticated: isAuthenticated,
    isAuthorized: isAuthorized,
}

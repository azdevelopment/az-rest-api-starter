const Express   = require('express')
const jwt       = require('jsonwebtoken')
const randToken = require('rand-token')
const User      = require('../resources/user/user.model')
const mongoose  = require('mongoose')

const router = Express.Router()

let refreshTokens = {}

router.post('/authenticate', (req, res, next) => {
    User.findOne({ email: req.body.email, password: req.body.password }, (err, user) => {
        if (err) return next(err);

        if (!user) {
            return res.sendStatus(401)
        } else {
            let signingUser = {
                '_id': mongoose.Types.ObjectId(user._id),
                'email': user.email,
                'admin': user.admin,
            }
            let token = jwt.sign(signingUser, 'somesecret', { expiresIn: 300 })
            let refreshToken = randToken.uid(256)
            refreshTokens[refreshToken] = signingUser

            return res.json({
                success: true,
                token: token,
                refreshToken: refreshToken
            })
        }
    })
})

router.post('/token', (req, res, next) => {
    let email = req.body.email
    let refreshToken = req.body.refreshToken

    if ((refreshToken in refreshTokens) && (refreshTokens[refreshToken].email === email)) {
        let signingUser = refreshTokens[refreshToken]
        let token = jwt.sign(signingUser, 'somesecret', { expiresIn: 300 })

        return res.json({
            success: true,
            token: token
        })
    } else {
        return res.sendStatus(401)
    }
})

router.post('/token/reject', (req, res, next) => {
    let refreshToken = req.body.refreshToken
    if (refreshToken in refreshTokens) {
        delete refreshTokens[refreshToken]
    }
    return res.sendStatus(204)
})

module.exports = router

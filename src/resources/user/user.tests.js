const request = require('supertest')
const { app } = require('../../server')
const auth    = require('../../auth/auth.lib')
const User    = require('./user.model')
const chai    = require('chai')

const should  = chai.should()
chai.use(require('chai-shallow-deep-equal'))

describe('/users', () => {
    let cleanUp = () =>
        User.remove({})

    beforeEach(cleanUp)
    after(cleanUp)

    describe('/', () => {
        describe('GET', () => {
            it('should return an array of users', () =>
                auth.getAdminToken().then(token =>
                    request(app)
                        .get('/users')
                        .set('x-access-token', token)
                        .expect(200)
                        .expect(res => {
                            res.body.length.should.equal(1)
                            res.body[0].should.shallowDeepEqual(auth.adminData)
                        })
                )
            )

            it('should return an error if not authenticated', () =>
                request(app)
                    .get('/users')
                    .set('x-access-token', 'abc')
                    .expect(401)
            )

            it('should return an error if not authorized', () =>
                auth.getUserToken().then(token =>
                    request(app)
                        .get('/users')
                        .set('x-access-token', token)
                        .expect(403)
                )
            )
        })

        describe('POST', () => {
            it('should save a new user', () =>
                auth.getAdminToken().then(token =>
                    request(app)
                        .post('/users')
                        .set('x-access-token', token)
                        .send(auth.userData)
                        .expect(200)
                        .then(res => {
                            res.body.should.shallowDeepEqual(auth.userData)
                            return User.findOne({ _id: res.body._id }).then(foundUser =>
                                foundUser.should.shallowDeepEqual(auth.userData)
                            )
                        })
                )
            )

            it('should return an error if not authenticated', () =>
                request(app)
                    .post('/users')
                    .set('x-access-token', 'abc')
                    .expect(401)
            )

            it('should return an error if not authorized', () =>
                auth.getUserToken().then(token =>
                    request(app)
                        .post('/users')
                        .set('x-access-token', token)
                        .expect(403)
                )
            )
        })
    })

    describe('/:id', () => {
        var testUser

        beforeEach(() =>
            User.create(auth.userData).then(user =>
                testUser = user
            )
        )

        describe('GET', () => {
            it('should return the object with matching id', () =>
                auth.getAdminToken().then(token =>
                    request(app)
                        .get(`/users/${testUser._id}`)
                        .set('x-access-token', token)
                        .expect(200)
                        .expect(res => {
                            res.body.should.shallowDeepEqual(auth.userData)
                            res.body._id.should.equal(testUser._id.toString())
                        })
                )
            )

            it('should return an error if not authenticated', () =>
                request(app)
                    .get(`/users/${testUser._id}`)
                    .set('x-access-token', 'abc')
                    .expect(401)
            )

            it('should return an error if not authorized', () =>
                auth.getUserToken().then(token =>
                    request(app)
                        .get(`/users/${testUser._id}`)
                        .set('x-access-token', token)
                        .expect(403)
                )
            )
        })

        describe('PUT', () => {
            var updatedUserData = {
                email: 'updated@email.az'
            }

            it('should update a user by id', () =>
                auth.getAdminToken().then(token =>
                    request(app)
                        .put(`/users/${testUser._id}`)
                        .set('x-access-token', token)
                        .send(updatedUserData)
                        .expect(200)
                        .then(res => {
                            res.body.should.shallowDeepEqual(updatedUserData)
                            return User.findOne({ _id: res.body._id }).then(foundUser => {
                                foundUser.should.not.shallowDeepEqual(auth.userData)
                                foundUser.should.shallowDeepEqual(updatedUserData)
                            })
                        })
                )
            )

            it('should return an error if not authenticated', () =>
                request(app)
                    .put(`/users/${testUser._id}`)
                    .set('x-access-token', 'abc')
                    .send(updatedUserData)
                    .expect(401)
            )

            it('should return an error if not authorized', () =>
                auth.getUserToken().then(token =>
                    request(app)
                        .get(`/users/${testUser._id}`)
                        .set('x-access-token', token)
                        .send(updatedUserData)
                        .expect(403)
                )
            )
        })

        describe('DELETE', () => {
            it('should remove a user by id', () =>
                auth.getAdminToken().then(token =>
                    request(app)
                        .delete(`/users/${testUser._id}`)
                        .set('x-access-token', token)
                        .expect(200)
                        .then(res => {
                            res.body.should.shallowDeepEqual(auth.userData)
                            return User.findOne({ _id: res.body._id }).then(foundUser => {
                                should.not.exist(foundUser)
                            })
                        })
                )
            )

            it('should return an error if not authenticated', () =>
                request(app)
                    .put(`/users/${testUser._id}`)
                    .set('x-access-token', 'abc')
                    .send({})
                    .expect(401)
            )

            it('should return an error if not authorized', () =>
                auth.getUserToken().then(token =>
                    request(app)
                        .get(`/users/${testUser._id}`)
                        .set('x-access-token', token)
                        .send({})
                        .expect(403)
                )
            )
        })
    })
})


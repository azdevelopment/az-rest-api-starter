let User = function() {
    let mongoose = require('mongoose')

    let schema = new mongoose.Schema({
        email: String,
        password: String,
        admin: Boolean,
        updated_at: { type: Date, default: Date.now },
    })

    return mongoose.model('User', schema)
}();

module.exports = User

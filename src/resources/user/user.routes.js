const Express = require('express')
const auth    = require('../../auth/auth.middleware')
const User    = require('./user.model')

const router = Express.Router()

router.collectionName = 'users'

router.get('/', auth.isAuthenticated, auth.isAuthorized, (req, res, next) => {
    User.find((err, found) => {
        if (err) return next(err);
        res.json(found)
    })
})

router.post('/', auth.isAuthenticated, auth.isAuthorized, (req, res, next) => {
    User.create(req.body, (err, created) => {
        if (err) return next(err);
        res.json(created)
    })
})

router.get('/:id', auth.isAuthenticated, auth.isAuthorized, (req, res, next) => {
    User.findById(req.params.id, (err, found) => {
        if (err) return next(err);
        res.json(found)
    })
})

router.put('/:id', auth.isAuthenticated, auth.isAuthorized, (req, res, next) => {
    User.findByIdAndUpdate(req.params.id, req.body, {new: true}, (err, updated) => {
        if (err) return next(err);
        res.json(updated)
    })
})

router.delete('/:id', auth.isAuthenticated, auth.isAuthorized, (req, res, next) => {
    User.findByIdAndRemove(req.params.id, (err, removed) => {
        if (err) return next(err);
        res.json(removed)
    })
})

module.exports = router

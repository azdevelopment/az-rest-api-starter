const Express = require('express')
const auth    = require('../../auth/auth.middleware')

const RockStar = require('./rock-star.model')

let router  = Express.Router()

router.collectionName = 'rock-stars'

router.get('/', auth.isAuthenticated, (req, res, next) =>
    RockStar.find((err, found) => {
        if (err) return next(err)
        return res.json(found)
    })
)

router.post('/', auth.isAuthenticated, (req, res, next) => {
    req.body.createdBy = req.user._id

    RockStar.create(req.body, (err, created) => {
        if (err) return next(err)
        return res.json(created)
    })
})

router.get('/:id', auth.isAuthenticated, (req, res, next) =>
    RockStar.findById(req.params.id, (err, found) => {
        if (err && err.name === 'CastError' && err.path === '_id' && !found) {
            return res.sendStatus(404)
        } else if (err) {
            return next(err)
        } else if (!found) {
            return res.sendStatus(404)
        }
        return res.json(found)
    })
)

router.put('/:id', auth.isAuthenticated, (req, res, next) =>
    RockStar.findById(req.params.id, (err, found) => {
        if (err && err.name === 'CastError' && err.path === '_id' && !found) {
            return res.sendStatus(404)
        } else if (err) {
            return next(err)
        } else if (!found) {
            return res.sendStatus(404)
        } else if (!found.createdBy.equals(req.user._id)) {
            return res.sendStatus(403)
        }

        Object.assign(found, req.body)
        found.save((err, saved) => {
            if (err) return next(err)
            return res.json(saved)
        })
    })
)

router.delete('/:id', auth.isAuthenticated, (req, res, next) =>
    RockStar.findById(req.params.id, (err, found) => {
        if (err && err.name === 'CastError' && err.path === '_id' && !found) {
            return res.sendStatus(404)
        } else if (err) {
            return next(err)
        } else if (!found) {
            return res.sendStatus(404)
        } else if (!found.createdBy.equals(req.user._id)) {
            return res.sendStatus(403)
        }

        found.remove((err, removed) => {
            if (err) return next(err)
            return res.json(removed)
        })
    })
)

module.exports = router

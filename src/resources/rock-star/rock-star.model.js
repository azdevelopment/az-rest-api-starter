const RockStar = (() => {
    let mongoose = require('mongoose')

    let schema = new mongoose.Schema({
        name: String,
        createdBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        updatedAt: { type: Date, default: Date.now },
    })

    return mongoose.model('RockStar', schema)
})()

module.exports = RockStar

const request  = require('supertest')
const app      = require('../../server').app
const auth     = require('../../auth/auth.lib')
const chai     = require('chai')

const should   = chai.should()
chai.use(require('chai-shallow-deep-equal'))

const RockStar = require('./rock-star.model')

describe('/rock-stars', () => {
    let rockStarData = {
        name: 'Test Name'
    }

    let cleanUp = () =>
        Promise.all([
            RockStar.remove({}),
            auth.remove(),
        ])

    beforeEach(cleanUp)
    after(cleanUp)

    describe('/', () => {
        describe('GET', () => {
            var createdRockStar

            beforeEach(() =>
                RockStar.create(rockStarData).then(created =>
                    createdRockStar = created
                )
            )

            it('should return an array of RockStar', () =>
                auth.getUserToken().then(token =>
                    request(app)
                        .get('/rock-stars')
                        .set('x-access-token', token)
                        .expect(200)
                        .expect(res => {
                            res.body.length.should.equal(1)
                            res.body[0].should.shallowDeepEqual(rockStarData)
                            res.body[0]._id.should.equal(createdRockStar._id.toString())
                        })
                )
            )

            it('should return an error if not authenticated', () =>
                request(app)
                    .get('/rock-stars')
                    .set('x-access-token', 'abc')
                    .expect(401)
            )
        })

        describe('POST', () => {
            it('should save a new RockStar', () =>
                auth.getTokenAndUser(auth.userData).then(data =>
                    request(app)
                        .post('/rock-stars')
                        .set('x-access-token', data.token)
                        .send(rockStarData)
                        .expect(200)
                        .then(res => {
                            res.body.should.shallowDeepEqual(rockStarData)

                            return RockStar.findOne({ _id: res.body._id }).then(found => {
                                found.should.shallowDeepEqual(rockStarData)
                                var createdByUser = found.createdBy.equals(data.user._id)
                                createdByUser.should.be.true
                            })
                        })
                )
            )

            it('should return an error if not authenticated', () =>
                request(app)
                    .post('/rock-stars')
                    .set('x-access-token', 'abc')
                    .expect(401)
            )
        })
    })

    describe('/:id', () => {
        let createdRockStar
        let owner
        let token

        beforeEach(() =>
            auth.getTokenAndUser(auth.userData).then(data => {
                owner = data.user
                token = data.token

                let withOwner = Object.assign({ createdBy: data.user._id }, rockStarData)
                return RockStar.create(withOwner).then(created =>
                    createdRockStar = created
                )
            })
        )

        describe('GET', () => {
            it('should return the RockStar with matching id', () =>
                request(app)
                    .get(`/rock-stars/${createdRockStar._id}`)
                    .set('x-access-token', token)
                    .expect(200)
                    .expect(res => {
                        res.body.should.shallowDeepEqual(rockStarData)
                        res.body._id.should.equal(createdRockStar._id.toString())
                    })
            )

            it('should return an error if not authenticated', () =>
                request(app)
                    .get(`/rock-stars/${createdRockStar._id}`)
                    .set('x-access-token', 'abc')
                    .expect(401)
            )

            it('should return an error if RockStar with that id doesn\'t exist', () =>
                request(app)
                    .get('/rock-stars/1111a1aa11a11a111a111a11')
                    .set('x-access-token', token)
                    .expect(404)
            )

            it('should return an error if the id isn\'t an ObjectId', () =>
                request(app)
                    .get('/rock-stars/123-not-an-object-id')
                    .set('x-access-token', token)
                    .expect(404)
            )
        })

        describe('PUT', () => {
            let updatedRockStarData = {
                name: 'Updated Name'
            }

            it('should update a RockStar by id', () =>
                request(app)
                    .put(`/rock-stars/${createdRockStar._id}`)
                    .set('x-access-token', token)
                    .send(updatedRockStarData)
                    .expect(200)
                    .then(res => {
                        res.body.should.shallowDeepEqual(updatedRockStarData)
                        return RockStar.findOne({ _id: res.body._id }).then(found => {
                            found.should.not.shallowDeepEqual(rockStarData)
                            found.should.shallowDeepEqual(updatedRockStarData)
                        })
                    })
            )

            it('should return an error if not authenticated', () =>
                request(app)
                    .put(`/rock-stars/${createdRockStar._id}`)
                    .set('x-access-token', 'abc')
                    .expect(401)
            )

            it('should return an error if RockStar is not createdBy user', () =>
                auth.getTokenAndUser(auth.otherUserData).then((other) =>
                    request(app)
                        .put(`/rock-stars/${createdRockStar._id}`)
                        .set('x-access-token', other.token)
                        .send(updatedRockStarData)
                        .expect(403)
                )
            )

            it('should return an error if RockStar with that id doesn\'t exist', () =>
                request(app)
                    .put('/rock-stars/1111a1aa11a11a111a111a11')
                    .set('x-access-token', token)
                    .send(updatedRockStarData)
                    .expect(404)
            )

            it('should return an error if the id isn\'t an ObjectId', () =>
                request(app)
                    .put('/rock-stars/123-not-an-object-id')
                    .set('x-access-token', token)
                    .send(updatedRockStarData)
                    .expect(404)
            )
        })

        describe('DELETE', () => {
            it('should remove a RockStar by id', () =>
                request(app)
                    .delete(`/rock-stars/${createdRockStar._id}`)
                    .set('x-access-token', token)
                    .expect(200)
                    .then(res => {
                        res.body.should.shallowDeepEqual(rockStarData)
                        return RockStar.findOne({ _id: res.body._id }).then(found =>
                            should.not.exist(found)
                        )
                    })
            )

            it('should return an error if not authenticated', () =>
                request(app)
                    .delete(`/rock-stars/${createdRockStar._id}`)
                    .set('x-access-token', 'abc')
                    .expect(401)
            )

            it('should return an error if RockStar is not createdBy user', () =>
                auth.getTokenAndUser(auth.otherUserData).then((other) =>
                    request(app)
                        .delete(`/rock-stars/${createdRockStar._id}`)
                        .set('x-access-token', other.token)
                        .expect(403)
                )
            )

            it('should return an error if RockStar with that id doesn\'t exist', () =>
                request(app)
                    .delete('/rock-stars/1111a1aa11a11a111a111a11')
                    .set('x-access-token', token)
                    .expect(404)
            )

            it('should return an error if the id isn\'t an ObjectId', () =>
                request(app)
                    .delete('/rock-stars/123-not-an-object-id')
                    .set('x-access-token', token)
                    .expect(404)
            )
        })
    })
})

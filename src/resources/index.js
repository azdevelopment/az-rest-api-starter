const fs = require('fs')

const directories = fs.readdirSync(__dirname, {})

directories.forEach(dir => {
    if (dir.indexOf('.') < 0) {
        module.exports[dir] = {
            model: require(`./${dir}/${dir}.model`),
            routes: require(`./${dir}/${dir}.routes`),
        }
    }
})

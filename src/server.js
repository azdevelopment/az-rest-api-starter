const Express    = require('express')
const bodyParser = require('body-parser')
const mongoose   = require('mongoose')
const resources  = require('./resources')
const authRoutes = require('./auth/auth.routes')

const app = Express()

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://mongodb/rest-api').then(() =>
    console.log('connection succesful')
).catch((err) =>
    console.error(err)
)

// create admin user if none exist

if (resources.user && resources.user.model) {
    let User = resources.user.model

    User.findOne({ admin: true }, (err, user) => {
        if (err) throw new Error(err)

        if (!user) {
            User.create({
                email: 'admin@email.az',
                password: 'admin',
                admin: true,
            })
        }
    })
}

// routes

app.get('/', (req, res) =>
    res.send('Welcome to a Rest Api')
)

Object.keys(resources).forEach((key) => {
    app.use(`/${resources[key].routes.collectionName}`, resources[key].routes)
})

app.use('/', authRoutes)

// errors

app.use((err, req, res, next) =>
    res.status(err.status || 500).send(err.message)
)

// server

const start = () =>
    app.listen(8080, () =>
        console.log('Listening on port 8080')
    )

module.exports = {
    app: app,
    start: start,
}

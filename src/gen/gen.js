const inquirer = require('inquirer')
const Mustache = require('mustache')
const path     = require('path')
const fs       = require('fs')
const mkdirp   = require('mkdirp')

const questions = [{
    name: 'resourceNameLower',
    message: 'Resource name (lower case, spaces between words)',
    default: 'rock star',
}, {
    name: 'resourceNameLowerPlural',
    message: 'Resource collection name (plural, lower case, spaces between words)',
    default: 'rock stars',
}]

const renderToFile = (templatePath, values, outputPath) =>
    new Promise((resolve, reject) =>
        fs.readFile(templatePath, 'UTF8', (err, template) => {
            if (err) reject(err)

            let output = Mustache.render(template, values)

            mkdirp(path.dirname(outputPath), err => {
                if (err) reject(err)

                fs.writeFile(outputPath, output, err =>
                    err ? reject(err) : resolve()
                )
            })

        })
    )

inquirer.prompt(questions).then(answers => {
    answers.resourceName = answers.resourceNameLower
        .split(' ')
        .map(part => part[0].toUpperCase() + part.slice(1).toLowerCase())
        .join('')

    answers.resourceNameCamel = answers.resourceName[0].toLowerCase() + answers.resourceName.slice(1)

    answers.resourceNameSlug = answers.resourceNameLower
        .split(' ')
        .join('-')
        .toLowerCase()

    answers.resourceNameSlugPlural = answers.resourceNameLowerPlural
        .split(' ')
        .join('-')
        .toLowerCase()

    let templatePath = templateType =>
        path.resolve(__dirname, `${templateType}.mustache`)

    let outputPath = templateType => {
        let slug = answers.resourceNameSlug
        let outputDir = path.resolve(__dirname, '..', 'resources', slug)
        return path.resolve(outputDir, `${slug}.${templateType}.js`)
    }

    return Promise.all([
        renderToFile(templatePath('model'), answers, outputPath('model')),
        renderToFile(templatePath('routes'), answers, outputPath('routes')),
        renderToFile(templatePath('tests'), answers, outputPath('tests')),
    ])
})

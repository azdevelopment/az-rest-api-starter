FROM node:6.10.2

RUN mkdir /rest-api
WORKDIR /rest-api

COPY package.json /rest-api
RUN npm install

COPY . /rest-api
EXPOSE 8080
CMD ["npm", "start"]
